const request = require('supertest');
const express = require('express');
const app = express();
const { expect } = require('chai');

const routes = require('../routes');

routes(app);

describe('Testes do getInfo', function() {
    it('Deve retornar status 200', async function () {
        const response = await request(app).get('/api/info');
        expect(response.status).equal(200);
    });

    it('Deve retornar success true', async function () {
        const response = await request(app).get('/api/info');
        expect(response.body.success).equal(true);
    });

    it('Deve retornar moreRecords true', async function () {
        const response = await request(app).get('/api/info');
        expect(response.body.moreRecords).equal(false);
    });

    it('Deve retornar data teste', async function () {
        const response = await request(app).get('/api/info');
        expect(response.body.data).equal('teste');
    });

});
