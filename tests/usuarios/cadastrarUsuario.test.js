const request = require('supertest');
const express = require('express');
const app = express();
const { expect } = require('chai');
const CONSTANTES = require('../../constants');
const routes = require('../../routes');

routes(app);

describe('Testes para cadastro de um usuário', () => {
    it('Deve retornar status 400 quando se envia a requisição incompleta', async function () {
        const response = await request(app).post('/api/usuario');
        expect(response.status).to.equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros).to.be.an('array').that.is.not.empty;
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com email inválido', async function () {
        const emailInvalido = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@',
            'telefone': '(44) 984629597',
            'senha': '123456',
            'acesso': '1',
            'idPessoa': 1
        };
        const response = await request(app).post('/api/usuario').send(emailInvalido);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.email_invalido);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com telefone inválido', async function () {
        const telefoneInvalido = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '777',
            'senha': '123456',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(telefoneInvalido);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.telefone_invalido);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com senha com menos de 6 caracteres', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': '123',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com senha com mais de 15 caracteres', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'TassyMelSamuel2011!',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com senha sem letra maiuscula', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'tassymel2011!',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com sem letra minuscula', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'TASSYMEL2011!',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com senha sem número', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'TassyMel!',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao tentar cadastrar um usuário com senha sem caracter especial', async function () {
        const senhaInvalida = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'TassyMel2011',
            'acesso': '1',
            'idPessoa': 1
        }
        const response = await request(app).post('/api/usuario').send(senhaInvalida);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.senha_invalida);
    });

    it('Deve retornar erro ao testar cadastrar um usuário com uma pessoa que não existe', async function () {
        const pessoaNaoCadastrada = {
            'nome': 'allan possani',
            'login': 'apcruz',
            'email': 'allanpcruz@gmail.com',
            'telefone': '(44) 98462-9597',
            'senha': 'TassyMel2011!',
            'acesso': '1',
            'idPessoa': 9999
        }
        const response = await request(app).post('/api/usuario').send(pessoaNaoCadastrada);
        expect(response.status).equal(400);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('error');
        expect(response.body.data.error).to.have.property('erros');
        expect(response.body.data.error.erros[0]).to.have.property('mensagem').that.is.equal(CONSTANTES.pessoa_nao_cadastrada);
    });

    // it('Deve retornar erro ao tentar cadastrar um usuário com login repetido', async function () {
    //     const novoUsuario = {
    //         'nome': 'allan possani 1',
    //         'login': 'apcruz 1',
    //         'email': 'allanpcruz+001@gmail.com',
    //         'telefone': '(11) 11111-1111',
    //         'senha': 'TassyMel2011!',
    //         'acesso': '1',
    //         'idPessoa': 1
    //     }
    //     const cadastroNovo = await request(app).post('/api/usuario').send(novoUsuario);
    //     const cadastroRepetido = await request(app).post('/api/usuario').send(novoUsuario);
    //     expect(cadastroRepetido.status).to.equal(400);
    //     const conflictErrors = cadastroRepetido.body.data.error.erros.filter(error => error.tipo === 'conflictError');
    //     expect(conflictErrors).to.have.lengthOf.at.least(1);
    //     conflictErrors.forEach(error => {
    //         expect(error).to.have.property('mensagem').that.is.oneOf([CONSTANTES.email_repetido, CONSTANTES.login_repetido, CONSTANTES.telefone_repetido]);
    //     });
    // });

    // it('Deve retornar erro ao tentar cadastrar um usuário com email repetido', async function () {
    //     const novoUsuario = {
    //         'nome': 'allan possani 2',
    //         'login': 'apcruz 2',
    //         'email': 'allanpcruz+002@gmail.com',
    //         'telefone': '(22) 22222-2222',
    //         'senha': 'TassyMel2011!',
    //         'acesso': '1',
    //         'idPessoa': 1
    //     }
    //     const cadastroNovo = await request(app).post('/api/usuario').send(novoUsuario);
    //     const cadastroRepetido = await request(app).post('/api/usuario').send(novoUsuario);
    //     expect(cadastroRepetido.status).to.equal(400);
    //     const conflictErrors = cadastroRepetido.body.data.error.erros.filter(error => error.tipo === 'conflictError');
    //     expect(conflictErrors).to.have.lengthOf.at.least(1);
    //     conflictErrors.forEach(error => {
    //         expect(error).to.have.property('mensagem').that.is.oneOf([CONSTANTES.email_repetido, CONSTANTES.login_repetido, CONSTANTES.telefone_repetido]);
    //     });
    // });

    // it('Deve retornar erro ao tentar cadastar um usuário com telefone repetido', async function () {
    //     const novoUsuario = {
    //         'nome': 'allan possani 3',
    //         'login': 'apcruz 3',
    //         'email': 'allanpcruz+003@gmail.com',
    //         'telefone': '(33) 33333-3333',
    //         'senha': 'TassyMel2011!',
    //         'acesso': '1',
    //         'idPessoa': 1
    //     }
    //     const cadastroNovo = await request(app).post('/api/usuario').send(novoUsuario);
    //     const cadastroRepetido = await request(app).post('/api/usuario').send(novoUsuario);
    //     expect(cadastroRepetido.status).to.equal(400);
    //     const conflictErrors = cadastroRepetido.body.data.error.erros.filter(error => error.tipo === 'conflictError');
    //     expect(conflictErrors).to.have.lengthOf.at.least(1);
    //     conflictErrors.forEach(error => {
    //         expect(error).to.have.property('mensagem').that.is.oneOf([CONSTANTES.email_repetido, CONSTANTES.login_repetido, CONSTANTES.telefone_repetido]);
    //     });
    // });

    // it('Deve retornar status 200 quando o usuário é gravado corretamente', async function () {
    //     const dadosValidos = {
    //         'nome': 'allan possani 4',
    //         'login': 'apcruz 4',
    //         'email': 'allanpcruz+004@gmail.com',
    //         'telefone': '(44) 44444-4444',
    //         'senha': 'TassyMel2011!',
    //         'acesso': 1,
    //         'idPessoa': 1
    //     };
    //     const response = await request(app).post('/api/usuario').send(dadosValidos);
    //     expect(response.status).equal(200);
    // });

});