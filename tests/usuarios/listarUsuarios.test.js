const request = require('supertest');
const express = require('express');
const app = express();
const { expect } = require('chai');
const CONSTANTES = require('../../constants');
const routes = require('../../routes');

routes(app);

describe('Testes para listar usuários', () => {
    it('Deve retornar status 200 e a lista de usuarios cadastrados', async function () {
        const response = await request(app).get('/api/usuarios');
        expect(response.status).to.equal(200);
        expect(response.body).to.have.property('data');
    })
});
