const request = require('supertest');
const express = require('express');
const app = express();
const { expect } = require('chai');
const CONSTANTES = require('../../constants');
const routes = require('../../routes');

routes(app);

describe('Testes para listar um usuario', () => {
    it('Deve retornar status 200 e um usuario cadastrado', async function () {
        const response = await request(app).get('/api/usuario/150');
        expect(response.status).to.equal(200);
        expect(response.body).to.have.property('data');
    });

    it('Deve retornar erro ao tentar listar sem o código do usuário', async function () {
        const response = await request(app).get('/api/usuario');
        expect(response.status).to.equal(404);
    });

    it('Deve retornar erro ao tentar listar usuário inexistente', async function () {
        const response = await request(app).get('/api/usuario');
        expect(response.status).to.equal(404);
    });
});
