const { validarSenha } = require('../../validators/senhaValidator');
const { expect } = require('chai');

describe('Testes para o validador de telefone', () => {
    it('Deve retornar true para uma senha com maiusculo, minusculo, numero, caracter especial, e com mais de 6 dígitos', () => {
        const senhaValida = 'TassyMel2011!';
        const resultado = validarSenha(senhaValida);
        expect(resultado).to.be.true;
    });

    it('Deve retornar false para uma senha sem minusculas', () => {
        const senhaInvalida = 'TASSYMEL2011!';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });
    
    it('Deve retornar false para uma senha sem maiusculas', () => {
        const senhaInvalida = 'tassymel2011!';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para uma senha sem numeros', () => {
        const senhaInvalida = 'TassyMel!';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para uma senha sem caracteres especiais', () => {
        const senhaInvalida = 'TassyMel2011';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para uma senha com menos de 6 dígitos', () => {
        const senhaInvalida = 'Tassy';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para uma senha com mais de 15 digitos', () => {
        const senhaInvalida = 'TassyMelSamuel2011!';
        const resultado = validarSenha(senhaInvalida);
        expect(resultado).to.be.false;
    });

});
