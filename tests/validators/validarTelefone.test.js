const { validarTelefone } = require('../../validators/telefoneValidator');
const { expect } = require('chai');

describe('Testes para o validador de telefone', () => {
    it('Deve retornar true para um telefone com 11 dígitos', () => {
        const telefoneValido = '(44) 12345-6789';
        const resultado = validarTelefone(telefoneValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar true para um telefone com 10 dígitos', () => {
        const telefoneValido = '(44) 1234-5678';
        const resultado = validarTelefone(telefoneValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar false para um telefone com 09 dígitos', () => {
        const telefoneValido = '(44) 1234-567';
        const resultado = validarTelefone(telefoneValido);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para um telefone com 12 dígitos', () => {
        const telefoneValido = '(44) 12345-67890';
        const resultado = validarTelefone(telefoneValido);
        expect(resultado).to.be.false;
    });
});
