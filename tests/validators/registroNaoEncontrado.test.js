const request = require('supertest');
const express = require('express');
const app = express();
const { expect } = require('chai');

const routes = require('../../routes');

routes(app);

describe('Testes para o validador CamposFaltando', () => {
    it('Deve retornar status 400', async function () {
        const response = await request(app).post('/api/usuario');
        expect(response.status).equal(400);
    });

    it('Deve retornar o objeto erro dentro do body', async function () {
        const response = await request(app).post('/api/usuario');
        expect(response.body.data.error).to.have.property('erros');
    });

    it('Deve retornar a propriedade name do erro como CamposInvalidos', async function () {
        const response = await request(app).post('/api/usuario');
        expect(response.body.data.error).to.have.property('name');
        expect(response.body.data.error.name).equal('CamposInvalidos');
    });

});
