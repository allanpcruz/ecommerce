const { validarEmail } = require('../../validators/emailValidator');
const { expect } = require('chai');

describe('Testes para o validador de e-mail', () => {
    it('Deve retornar true para um e-mail válido', () => {
        const emailValido = 'exemplo@email.com';
        const resultado = validarEmail(emailValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar true para um e-mail válido com subdomínio', () => {
        const emailValido = 'usuario@subdominio.dominio.com';
        const resultado = validarEmail(emailValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar true para um e-mail válido com domínio de dois caracteres', () => {
        const emailValido = 'usuario@exemplo.co';
        const resultado = validarEmail(emailValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar true para um e-mail válido com caracteres especiais', () => {
        const emailValido = 'usuario+teste@example.com';
        const resultado = validarEmail(emailValido);
        expect(resultado).to.be.true;
    });

    it('Deve retornar false para um e-mail com dominio inválido', () => {
        const emailInvalido = 'usuario@br.br';
        const resultado = validarEmail(emailInvalido);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para um e-mail inválido sem "@"', () => {
        const emailInvalido = 'email.invalido';
        const resultado = validarEmail(emailInvalido);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para um e-mail inválido sem domínio', () => {
        const emailInvalido = 'usuario@';
        const resultado = validarEmail(emailInvalido);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para um e-mail inválido com espaços', () => {
        const emailInvalido = 'email com espaco@example.com';
        const resultado = validarEmail(emailInvalido);
        expect(resultado).to.be.false;
    });

    it('Deve retornar false para um e-mail inválido com domínio de apenas um caractere', () => {
        const emailInvalido = 'usuario@e.com';
        const resultado = validarEmail(emailInvalido);
        expect(resultado).to.be.false;
    });

});
