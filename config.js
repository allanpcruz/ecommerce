require('dotenv/config');
module.exports = Object.freeze(
    {
        app_port: process.env.APP_PORT,
        api_version: process.env.API_VERSION,
        registros_pagina: process.env.REGISTROS_PAGINA,
        status_ativo: process.env.STATUS_ATIVO,
        status_inativo: process.env.STATUS_INATIVO,
        bcrypt_rounds: process.env.BCRYPT_ROUNDS,
        acesso_admin: process.env.ACESSO_ADMIN,
        acesso_finan_admin: process.env.ACESSO_FINAN_ADMIN,
        acesso_finan_pagar: process.env.ACESSO_FINAN_PAGAR,
        acesso_finan_receb: process.env.ACESSO_FINAN_RECEB,
        acesso_profe: process.env.ACESSO_PROFE,
        acesso_aluno: process.env.ACESSO_ALUNO,
    }
);
