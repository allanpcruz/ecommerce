const database = require('../models');
const modelos = database.sequelize.models;
const logger = require('../log');
const { sequelize } = require('../models');

class Repository {
    constructor(nomeDoModelo) {
        this.nomeDoModelo = nomeDoModelo;
    }

    async criarTransacao() {
        try {
            return await database.sequelize.transaction();
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async pegarRegistros(options = {}) {
        const where = options.where ?? {};
        const offset = parseInt(options.offset, 10) ?? 0;
        const limit = parseInt(options.limit, 10) ?? 100;
        const order = options.order ?? [['id', 'ASC']];
        const attributes = options.attributes ?? '';
        const group = options.group ?? null;
        try {
            return await modelos[this.nomeDoModelo].findAll({
                where: { ...where },
                offset: parseInt(offset, 10),
                limit: parseInt(limit, 10),
                order,
                attributes,
                group
            });
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async pegarTodosOsRegistros(options = {}) {
        const where = options.where ?? {};
        const order = options.order ?? [['id', 'ASC']];
        const attributes = options.attributes ?? '';
        try {
            return await modelos[this.nomeDoModelo].findAll(
                {
                    where: { ...where },
                    order,
                    attributes
                }
            );
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async totalRegistros(options = {}) {
        const where = options.hasOwnProperty('where') ? options.where : {};
        try {
            return modelos[this.nomeDoModelo].count({ where: { ...where } });
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async encontrarUmRegistro(where = {}, literalSql = false) {
        try {
            const options = {
                where: { ...where }
            };
            if (literalSql) {
                options.where = sequelize.literal(literalSql);
            }
            return await modelos[this.nomeDoModelo].findOne(options);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async criarRegistro(dados) {
        try {
            return await modelos[this.nomeDoModelo].create(dados);
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    async atualizarRegistro(dados = {}, transacao) {
        const id = dados.hasOwnProperty('id') ? dados.id : 0;
        const dadosAtualizar = dados.hasOwnProperty('dadosAtualizar') ? dados.dadosAtualizar : {};
        if (id === 0 || Object.keys(dadosAtualizar).length === 0) {
            return false;
        }
        try {
            return modelos[this.nomeDoModelo].update(
                dadosAtualizar,
                { where: { id: Number(id) } },
                transacao,
            );

        } catch (error) {
            logger.error(error);
            throw error;
        }
    }
}

module.exports = Repository;
