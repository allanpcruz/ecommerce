const config = require('../config');
const constantes = require('../constants');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        return res.status(401).json({ msgErro: constantes.nao_autorizado });
    }

    try {
        const token = authHeader.split(' ')[1];
        const decodedToken = jwt.verify(token, config.api_secret);
        req.user = decodedToken;
        req.body.contratosLiberados = decodedToken.contratos;
        return next();
    } catch (error) {
        return res.status(401).json({ msgErro: constantes.nao_autorizado });
    }
};
