module.exports = (_req, res, next) => {
    res.setResponse = (data, options = {}) => {
        const statusCode = options.statusCode || 200;
        const success = options.success;
        const moreRecords = options.moreRecords || false;
        const dataReturn = {
            success,
            moreRecords,
            data,
        };
        if (statusCode === 201) {
            res.status(statusCode).end();
        } else {
            res.status(statusCode).json(dataReturn);
        }
    };
    next();
};
