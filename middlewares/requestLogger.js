const fs = require('fs');
const path = require('path');
const moment = require('moment');

const currentDate = moment().format('YYYY-MM-DD')
const logDirectory = path.join(__dirname, '../log/data');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

function requestLogger(req, _res, next) {
    const logFileName = `request-${currentDate}.log`
    const logFilePath = path.join(logDirectory, logFileName);

    const logData = {
        timestamp: new Date().toISOString(),
        method: req.method,
        url: req.url,
        headers: req.headers,
        body: req.body,
    };

    const logEntry = JSON.stringify(logData, null, 2) + '\n';

    fs.appendFileSync(logFilePath, logEntry);

    next();

}

module.exports = requestLogger;