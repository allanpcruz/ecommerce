'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PessoaJuridica extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PessoaJuridica.init({
    idPessoa: DataTypes.INTEGER,
    razaoSocial: DataTypes.STRING,
    nomeFantasia: DataTypes.STRING,
    cnpj: DataTypes.STRING,
    cnae: DataTypes.STRING,
    dataConstituicao: DataTypes.BIGINT,
    representanteLegal: DataTypes.STRING,
    cpfRepresentante: DataTypes.STRING,
    telefoneRepresentante: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PessoaJuridica',
  });
  return PessoaJuridica;
};