'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Telefones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Telefones.init({
    idPessoa: DataTypes.INTEGER,
    telefone: DataTypes.STRING,
    tipoTelefone: DataTypes.INTEGER,
    principal: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Telefones',
  });
  return Telefones;
};