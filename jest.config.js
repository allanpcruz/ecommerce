module.exports = {
    testEnvironment: 'node',
    setupFiles: ['./jest.setup.js'],
    testMatch: ['<rootDir>/tests/**/*.test.js'],
    detectOpenHandles: true,
};
