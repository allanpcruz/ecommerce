module.exports = Object.freeze(
    {
        acesso_obrigatorio: 'O campo acesso é obrigatório.',
        email_obrigatorio: 'O campo e-mail é obrigatório.',
        email_invalido: 'Campo e-mail inválido.',
        email_repetido: 'Campo e-mail já existe para outro usuário.',
        erro_conflito: 'conflictError',
        erro_invalido: 'badRequest',
        idPessoa_obrigatorio: 'O campo idPessoa é obrigatório.',
        login_obrigatorio: 'O campo login é obrigatório.',
        login_repetido: 'Campo login já existe para outro usuário.',
        nome_obrigatorio: 'O campo nome é obrigatório.',
        pessoa_nao_cadastrada: 'Pessoa informada não cadastrada',
        senha_invalida: 'Campo senha inválido',
        senha_obrigatorio: 'O campo senha é obrigatório.',
        status_obrigatorio: 'O campo status é obrigatório.',
        telefone_obrigatorio: 'O campo telefone é obrigatório',
        telefone_invalido: 'Campo telefone inválido',
        telefone_repetido: 'Campo telefone já existe para outro usuário',
    }
);