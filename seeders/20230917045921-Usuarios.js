'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const users = [];

    for (let i = 1; i <= 50; i++) {
      users.push({
        nome: `Usuario ${i}`,
        login: `user${i}`,
        email: `user${i}@example.com`,
        telefone: `(123) 456-789${i.toString().padStart(2, '0')}`,
        senha: 'senha123', // Você pode usar senhas fictícias aqui
        status: 1, // Status ativo
        acesso: 1, // Nível de acesso
        ultimoAcesso: Date.now(), // Último acesso fictício
        idPessoa: i, // ID de pessoa fictício
        createdAt: new Date(),
        updatedAt: new Date(),
      })
    }

    await queryInterface.bulkInsert('Usuarios', users, {});

    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Usuarios', null, {});

  }
};
