function onRequestReceived(req, res, next) {
    const ambiente = process.env.NODE_ENV || 'develop';
    res.send(`<h1>API e-commerce</h1> <h2>Ambiente: ${ambiente}</h2>`);
}

module.exports = onRequestReceived;
