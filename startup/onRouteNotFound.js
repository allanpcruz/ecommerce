function onRouteNotFound(req, res, next) {
    res.status(404).json({ msgErro: 'Rota inválida ou inexistente' });
}

module.exports = onRouteNotFound;
