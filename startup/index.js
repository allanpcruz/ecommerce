const onRequestReceived = require('./onRequestReceived');
const onRouteNotFound = require('./onRouteNotFound');

module.exports = {
    onRequestReceived,
    onRouteNotFound,
};
