const { Router } = require('express');
const PessoasController = require('../controllers/PessoasController.js');
const router = Router();

router.post('/api/login', PessoasController.login);

router.get('/api/pessoas', PessoasController.getPessoas);

module.exports = router;
