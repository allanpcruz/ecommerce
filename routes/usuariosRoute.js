const { Router } = require('express');
const UsuariosController = require('../controllers/UsuariosController.js');
const router = Router();

router.post('/api/usuario', UsuariosController.cadastrarUsuario);
router.get('/api/usuarios', UsuariosController.listarUsuarios);
router.get('/api/usuario/:id', UsuariosController.listarUsuario);
router.put('/api/usuario/:id/credenciais', UsuariosController.alterarCredenciaisUsuario);

module.exports = router;
