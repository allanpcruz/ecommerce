const { Router } = require('express');
const InfoController = require('../controllers/InfoController.js');
const router = Router();

router.get('/api/info', InfoController.getInfo);

module.exports = router;
