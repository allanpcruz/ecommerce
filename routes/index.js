const bodyParser = require('body-parser');
const setResponse = require('../middlewares/setResponse');

const info = require('./infoRoute');
const pessoas = require('./pessoasRoute.js');
const usuarios = require('./usuariosRoute')

module.exports = app => {
    app.use(
        setResponse,
        bodyParser.json(),
        info,
        pessoas,
        usuarios,
    );
};
