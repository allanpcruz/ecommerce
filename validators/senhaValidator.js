function validarSenha(senha) {
    const regexSenha = /^(?=.*[A-Z])(?=.*[!#@$%&])(?=.*[0-9])(?=.*[a-z]).{6,15}$/

    return regexSenha.test(senha);
}

module.exports = { validarSenha };