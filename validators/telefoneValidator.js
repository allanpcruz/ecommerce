function validarTelefone(telefone) {
    const numeros = telefone.replace(/\D/g, '');

    if (numeros.length === 10 || numeros.length === 11) {
        return true;
    }

    return false;
}

module.exports = { validarTelefone };