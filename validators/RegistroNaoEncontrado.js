class RegistroNaoEncontrado extends Error {
    constructor(mensagem) {
        super(mensagem || 'Registro não encontrado');
        this.name = 'RegistroNaoEncontrado';
        this.status = 404;
    }
}

module.exports = RegistroNaoEncontrado;
