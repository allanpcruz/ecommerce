const validator = require('email-validator');
function validarEmail(email) {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    if (!emailRegex.test(email)) {
        return false;
    }

    if (email.endsWith('..')) {
        return false;
    }

    const partes = email.split('@');
    const dominio = partes[1];

    if (dominio.length === 5) {
        return false;
    }

    return validator.validate(email);
}

module.exports = { validarEmail };
