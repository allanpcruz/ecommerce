class CamposFaltando extends Error {
    constructor(erros) {
        super('Parâmetros inválidos');
        this.name = 'CamposInvalidos';
        this.status = 400;
        this.erros = erros;
    }
}

module.exports = CamposFaltando;
