const config = require('./config');
const routes = require('./routes');
const setResponse = require('./middlewares/setResponse');
const startup = require('./startup');
const express = require('express');
const moment = require('moment');
const requestLogger = require('./middlewares/requestLogger.js');
const bodyParser = require('body-parser');
const port = config.app_port;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(requestLogger)

app.use(setResponse);

routes(app);

app.get('/', startup.onRequestReceived);

app.use(startup.onRouteNotFound);

const server = app.listen(port, () => {
    console.log(`Servidor iniciado em ${moment().format('DD/MM/YYYY')} às ${moment().format('HH:mm:ss')} na porta ${port}`);
});
