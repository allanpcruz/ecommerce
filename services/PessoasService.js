const Repositories = require('../repositories')
const pessoaRepository = new Repositories('Pessoas');

class PessoasService {
    static async login(_options) {
        return { data: 'data' };
    }
    
    static async getPessoas(_options = {}) {
        const registros = await pessoaRepository.pegarTodosOsRegistros();

        return { data: registros };
    }
}

module.exports = PessoasService;
