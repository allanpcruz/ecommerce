const Repositories = require('../repositories')
const usuarioRepository = new Repositories('Usuarios');
const CamposFaltando = require('../validators/CamposFaltando');
const RegistroNaoEncontrado = require('../validators/RegistroNaoEncontrado');
const { validarEmail } = require('../validators/emailValidator');
const { validarTelefone } = require('../validators/telefoneValidator');
const { validarSenha } = require('../validators/senhaValidator');
const CONSTANTES = require('../constants');
const CONFIG = require('../config');
const bcrypt = require('bcryptjs');

class UsuarioService {
    static async cadastrarUsuario(options) {
        let { nome, login, email, telefone, senha, status, acesso, idPessoa } = options
        const erros = [];

        if (!nome) {
            erros.push({ campo: 'nome', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.nome_obrigatorio });
        }

        if (!login) {
            erros.push({ campo: 'login', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.login_obrigatorio });
        } else {
            const loginRepetido = await usuarioRepository.encontrarUmRegistro({ login })
            if (loginRepetido !== null) {
                erros.push({ campo: 'login', tipo: CONSTANTES.erro_conflito, mensagem: CONSTANTES.login_repetido });
            }
        }

        if (!email) {
            erros.push({ campo: 'email', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.email_obrigatorio });
        } else {
            if (!validarEmail(email)) {
                erros.push({ campo: 'email', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.email_invalido });
            } else {
                const emailRepetido = await usuarioRepository.encontrarUmRegistro({ email });
                if (emailRepetido !== null) {
                    erros.push({ campo: 'email', tipo: CONSTANTES.erro_conflito, mensagem: CONSTANTES.email_repetido });
                }
            }
        }

        if (!telefone) {
            erros.push({ campo: 'telefone', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.telefone_obrigatorio });
        } else {
            if (!validarTelefone(telefone)) {
                erros.push({ campo: 'telefone', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.telefone_invalido });
            } else {
                const telefoneRepetido = await usuarioRepository.encontrarUmRegistro({ telefone });
                if (telefoneRepetido !== null) {
                    erros.push({ campo: 'telefone', tipo: CONSTANTES.erro_conflito, mensagem: CONSTANTES.telefone_repetido });
                }
            }
        }

        if (!senha) {
            erros.push({ campo: 'senha', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.senha_obrigatorio });
        } else {
            if (!validarSenha(senha)) {
                erros.push({ campo: 'senha', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.senha_invalida });
            }
        }

        if (!acesso) {
            erros.push({ campo: 'acesso', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.acesso_obrigatorio });
        }

        if (!idPessoa) {
            erros.push({ campo: 'idPessoa', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.idPessoa_obrigatorio });
        } else {
            const pessoa = await usuarioRepository.encontrarUmRegistro({ idPessoa });
            if (pessoa === null) {
                erros.push({ campo: 'idPessoa', tipo: CONSTANTES.erro_invalido, mensagem: CONSTANTES.pessoa_nao_cadastrada });
            }
        }

        if (erros.length > 0) {
            throw new CamposFaltando(erros);
        }

        const dadosGravar = {
            nome,
            login,
            email,
            telefone,
            senha,
            status: CONFIG.status_ativo,
            acesso,
            idPessoa,
        }

        dadosGravar.senha = bcrypt.hashSync(
            dadosGravar.senha,
            bcrypt.genSaltSync(parseInt(CONFIG.bcrypt_rounds, 10))
        )

        const novoUsuario = await usuarioRepository.criarRegistro(dadosGravar);

        delete novoUsuario.dataValues.senha;

        return novoUsuario;
    }

    static async listarUsuarios(_options) {
        const usuarios = await usuarioRepository.pegarTodosOsRegistros();
        return { data: usuarios };
    }

    static async listarUsuarioPorId(idUsuario) {
        const usuario = await usuarioRepository.encontrarUmRegistro({ id: idUsuario });
        if (!usuario) {
            throw new RegistroNaoEncontrado();
        }
        return { data: usuario };
    }

    static async alterarCredenciaisUsuario(options) {
    }

}

module.exports = UsuarioService;
