'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PessoaJuridicas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idPessoa: {
        type: Sequelize.INTEGER
      },
      razaoSocial: {
        type: Sequelize.STRING
      },
      nomeFantasia: {
        type: Sequelize.STRING
      },
      cnpj: {
        type: Sequelize.STRING
      },
      cnae: {
        type: Sequelize.STRING
      },
      dataConstituicao: {
        type: Sequelize.BIGINT
      },
      representanteLegal: {
        type: Sequelize.STRING
      },
      cpfRepresentante: {
        type: Sequelize.STRING
      },
      telefoneRepresentante: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PessoaJuridicas');
  }
};