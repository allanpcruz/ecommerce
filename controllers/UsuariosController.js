const logger = require('../log');
const UsuariosService = require('../services/UsuariosService');

class UsuariosController {
    static async cadastrarUsuario(req, res) {
        try {
            const response = await UsuariosService.cadastrarUsuario(req.body);
            return res.setResponse({ success: true, data: response.data });
        } catch (error) {
            var mensagem = error;
            if (error.status >= 500) {
                logger.error(error);
                mensagem = 'Ocorreu um erro interno do servidor.';
            }
            return res.setResponse({ success: false, error: mensagem }, { statusCode: error.status });
        }
    }

    static async listarUsuarios(req, res) {
        try {
            const response = await UsuariosService.listarUsuarios(req.body);
            return res.setResponse({ success: true, data: response.data });
        } catch (error) {
            var mensagem = error;
            if (error.status >= 500) {
                logger.error(error);
                mensagem = 'Ocorreu um erro interno do servidor.';
            }
            return res.setResponse({ success: false, error: mensagem }, { statusCode: error.status });
        }
    }

    static async listarUsuario(req, res) {
        try {
            const response = await UsuariosService.listarUsuarioPorId(req.params.id);
            return res.setResponse({ success: true, data: response.data });
        } catch (error) {
            var mensagem = error;
            if (error.status >= 500) {
                logger.error(error);
                mensagem = 'Ocorreu um erro interno do servidor.';
            }
            return res.setResponse({ success: false, error: mensagem }, { statusCode: error.status });
        }
    }

    static async alterarCredenciaisUsuario(req, res) {
        try {
            const response = await UsuariosService.alterarCredenciaisUsuario({ id: req.params.id, credenciais: req.body });
            return res.setResponse({ success: true, data: response.data });
        } catch (error) {
            var mensagem = error;
            if (error.status >= 500) {
                logger.error(error);
                mensagem = 'Ocorreu um erro interno do servidor';
            }
            return res.setResponse({ success: false, error: mensagem }, { statusCode: error.status });
        }
    }

}

module.exports = UsuariosController;