const logger = require('../log');
const PessoasService = require('../services/PessoasService');

class PessoasController {
    static async login(req, res) {
        try {
            const response = await PessoasService.login(req.body);
            if (response.hasOwnProperty('Erro')) {
                return res.setResponse(response, { statusCode: 400, success: false });
            }   
            return res.setResponse(response.data, { statusCode: 200, success: true });
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

    static async getPessoas(req, res) {
        try {
            const response = await PessoasService.getPessoas(req.body);
            if (response.hasOwnProperty('Erro')) {
                return res.setResponse(response, { statusCode: 400, success: false });
            }
            return res.setResponse(response.data, { statusCode: 200, success: true });
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

}

module.exports = PessoasController;
