const logger = require('../log');
const InfoService = require('../services/InfoService');

class InfoController {
    static async getInfo(req, res) {
        try {
            const response = await InfoService.getInfo(req.body);
            if (response.hasOwnProperty('Erro')) {
                return res.setResponse(response, { statusCode: 400, success: false });
            }
            return res.setResponse(response.data, { statusCode: 200, success: true });
        } catch (error) {
            logger.error(error);
            throw error;
        }
    }

}

module.exports = InfoController;
